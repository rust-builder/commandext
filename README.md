# commandext

A Command extension suitable for use by Rust Builders.

## Version

[![Crates.io](https://img.shields.io/crates/v/commandext.svg)](https://crates.io/crates/commandext)
[![Build Status](https://travis-ci.org/rust-builder/commandext.svg?branch=master)](travis)

## License

Licensed under either of
 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)
at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any
additional terms or conditions.
